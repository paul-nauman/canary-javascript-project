$(function(){
    //Create Game Canvas
    CanaryGame = {
        "KeyCodes": { "enter": 13, "left": 37, "up": 38, "right": 39, "down": 40 },
        "GameCanvas": $("#game_canvas"),
        "GameCanvasContext": $("#game_canvas")[0].getContext("2d"),
        "GameCanvasWidth": $("#game_canvas").width(),
        "GameCanvasHeight": $("#game_canvas").height(),
        "GameInitialized": false,
        "GameOver": false
    };
    
    //Load High Scores
    loadHighScores();
    
    //Create Initial Text
    CanaryGame.GameCanvasContext.font = "30px Arial";
    CanaryGame.GameCanvasContext.fillStyle = "black";
    CanaryGame.GameCanvasContext.textAlign = "center";
    CanaryGame.GameCanvasContext.fillText("Press ENTER to start", CanaryGame.GameCanvasWidth/2, CanaryGame.GameCanvasHeight/2);
    
    //Capture Keyboard Inputs
    $(document).keydown(function(e){
        //Add check for when focus is on a input
        const tag = e.target.tagName.toLowerCase();
        
        switch(e.keyCode) {
            case CanaryGame.KeyCodes.enter:
                if(tag != 'input' && $("#game_score_input").length == 0) {
                    if(CanaryGame.GameInitialized) return;
            
                    //Start the game
                    initializeGame();
                }
            break;
            case CanaryGame.KeyCodes.left:
                if(tag != 'input' && $("#game_score_input").length == 0) {
                    if(!CanaryGame.GameInitialized) return;
                    
                    //Move cursor to the left
                    if(CanaryGame.GameCursor.position[0] < CanaryGame.GameBoundry.position[0] || CanaryGame.GameCursor.position[0] > CanaryGame.GameBoundry.position[0] + CanaryGame.GameBoundry.width) {
                        gameOver(animation);
                    } else {
                        CanaryGame.GameCursor.position[0] = CanaryGame.GameCursor.position[0]-10;
                        
                        CanaryGame.GameCursor.animate();
                    }
                }
            break;
            case CanaryGame.KeyCodes.up:
                if(tag != 'input' && $("#game_score_input").length == 0) {
                    if(!CanaryGame.GameInitialized) return;
                    
                    //Move the cursor upwards 
                    if(CanaryGame.GameCursor.position[1] > CanaryGame.GameBoundry.position[1] + CanaryGame.GameBoundry.height || CanaryGame.GameCursor.position[1] < CanaryGame.GameBoundry.position[1]) {
                        gameOver(animation);
                    } else {
                        CanaryGame.GameCursor.position[1] = CanaryGame.GameCursor.position[1]-10;
                        
                        CanaryGame.GameCursor.animate();
                    }
                }
            break;
            case CanaryGame.KeyCodes.right:
                if(tag != 'input' && $("#game_score_input").length == 0) {
                    if(!CanaryGame.GameInitialized) return;
                    
                    //Move the cursor to the right
                    if(CanaryGame.GameCursor.position[0] < CanaryGame.GameBoundry.position[0] || CanaryGame.GameCursor.position[0] > CanaryGame.GameBoundry.position[0] + CanaryGame.GameBoundry.width) {
                        gameOver(animation);
                    } else {
                        CanaryGame.GameCursor.position[0] = CanaryGame.GameCursor.position[0]+10;
                        
                        CanaryGame.GameCursor.animate();
                    }
                }
            break;
            case CanaryGame.KeyCodes.down:
                if(tag != 'input' && $("#game_score_input").length == 0) {
                    if(!CanaryGame.GameInitialized) return;
                    
                    //Move cursor downwards
                    if(CanaryGame.GameCursor.position[1] > CanaryGame.GameBoundry.position[1] + CanaryGame.GameBoundry.height || CanaryGame.GameCursor.position[1] < CanaryGame.GameBoundry.position[1]) {
                        gameOver(animation);
                    } else {
                        CanaryGame.GameCursor.position[1] = CanaryGame.GameCursor.position[1]+10;
                        
                        CanaryGame.GameCursor.animate();
                    }
                }
            break;    
        }
    });
});

//Animate all objects
function animation() {
	if(CanaryGame.GameOver) return;
	
	//Clear the canvas
	CanaryGame.GameCanvasContext.clearRect(CanaryGame.GameBoundry.position[0],CanaryGame.GameBoundry.position[1],CanaryGame.GameBoundry.width,CanaryGame.GameBoundry.height);
	
	//Loop through the ball array and animate
	for(let i=0; i<CanaryGame.GameBalls.length; i++) {
		CanaryGame.GameBalls[i].animate();
	}
	
	//Aniamte cursor
	CanaryGame.GameCursor.animate();
	
	//Start Animations
	requestAnimationFrame(animation);
}

//Initialize the game
function initializeGame() {
    $("#game_ball_counter span").text(0);
    
    CanaryGame.GameInitialized = true;
    CanaryGame.GameOver = false;
    
    //Create Game Boundry
    CanaryGame.GameBoundry = new Boundry();
    
    //Create Game Cursor
    CanaryGame.GameCursor = new Cursor();
    
    //Create Balls Array
    CanaryGame.GameBalls = [];
    
    //Create a inital ball
    CanaryGame.GameBalls.push(new Ball());
    
    createBall = setInterval(function() {
        CanaryGame.GameBalls.push(new Ball());
    }, 3000);
        
    //Start Animations
    requestAnimationFrame(animation);
}

//Show Game Over screen
function gameOver(animation) {
    CanaryGame.GameInitialized = false;
    CanaryGame.GameOver = true;
    
    //Clear all loops
    clearInterval(createBall);
    cancelAnimationFrame(animation);
    
    //Clear the canvas
    CanaryGame.GameCanvasContext.clearRect(CanaryGame.GameBoundry.position[0],CanaryGame.GameBoundry.position[1],CanaryGame.GameBoundry.width,CanaryGame.GameBoundry.height);

    //Check to see if score should be recorded
    checkHighScore();
}

//Check high score
function checkHighScore() {
    if(typeof(Storage) !== "undefined") {
        const gameHighScores = JSON.parse(localStorage.getItem("GameHighScores"));
        let maxScore = 0;
        
        //Find the max score from the scores list
        for(let i=0; i<gameHighScores.length; i++) {
            if(gameHighScores[i][1] > maxScore) maxScore = gameHighScores[i][1];            
        }
        
        //If current score is more then the ma from the score list
        if(maxScore < Number($("#game_ball_counter span").text())) {
            //Create a new score
            createHighScore();
        } else {
            //Show Game Over screen
            CanaryGame.GameCanvasContext.font = "30px Arial";
            CanaryGame.GameCanvasContext.fillStyle = "black";
            CanaryGame.GameCanvasContext.textAlign = "center";
            
            const text = "Game Over\nPress ENTER to restart\n";
            const lines = text.split('\n');
            
            for(let i=0; i<lines.length; i++) {
              CanaryGame.GameCanvasContext.fillText(lines[i], CanaryGame.GameCanvasWidth/2, CanaryGame.GameCanvasHeight/2 + (i * 35));
            }        
        }
    }
}

//Create high score
function createHighScore() {
    CanaryGame.GameCanvasContext.font = "30px Arial";
    CanaryGame.GameCanvasContext.fillStyle = "black";
    CanaryGame.GameCanvasContext.textAlign = "center";
    
    const text = "Game Over\nEnter your name to add a high score\n";
    const lines = text.split('\n');
    
    for(let i=0; i<lines.length; i++) {
      CanaryGame.GameCanvasContext.fillText(lines[i], CanaryGame.GameCanvasWidth/2, CanaryGame.GameCanvasHeight/2 + (i * 35));
    } 
    
    //Create input for user
    $("#game_area").append('<input type="text" id="game_score_input" name="game_score_input" />');
    
    $("#game_score_input").keydown(function(e){
        switch(e.keyCode) {
            case CanaryGame.KeyCodes.enter:
                if($(this).val() != "") {
                    if(typeof(Storage) !== "undefined") {
                        //Add high score and store it to localStorage
                        const gameHighScores = JSON.parse(localStorage.getItem("GameHighScores"));
    
                        if(Number($("#game_ball_counter span").text()) > 0) {
                            gameHighScores.push([$(this).val(), Number($("#game_ball_counter span").text())]);
                        }
                
                        //Sort scores to display highest number first
                        gameHighScores.sort(sortHighScores);
                        
                        //Display high score list
                        if(gameHighScores.length > 0) {
                            $("#game_high_scores ul").empty();
                        }
                        
                        for(let i=0; i<gameHighScores.length; i++) {
                		    $("#game_high_scores ul").append("<li>"+gameHighScores[i][0]+": "+gameHighScores[i][1]+"</li>");
                        }
                        
                        localStorage.setItem("GameHighScores", JSON.stringify(gameHighScores));
                        
                        $("#game_score_input").remove();
                        
                        //Show inital start screen
                        CanaryGame.GameCanvasContext.clearRect(CanaryGame.GameBoundry.position[0],CanaryGame.GameBoundry.position[1],CanaryGame.GameBoundry.width,CanaryGame.GameBoundry.height);
                        
                        CanaryGame.GameCanvasContext.font = "30px Arial";
                        CanaryGame.GameCanvasContext.fillStyle = "black";
                        CanaryGame.GameCanvasContext.textAlign = "center";
                        CanaryGame.GameCanvasContext.fillText("Press ENTER to start", CanaryGame.GameCanvasWidth/2, CanaryGame.GameCanvasHeight/2);
                    }
                }
            break;   
        }
    });
}

//Load in high scores
function loadHighScores() {
    if(typeof(Storage) !== "undefined") {
        if(localStorage.getItem("GameHighScores") === null) {
            const gameHighScores = [];
            
            localStorage.setItem("GameHighScores", JSON.stringify(gameHighScores));
        } else {
            const gameHighScores = JSON.parse(localStorage.getItem("GameHighScores"));
            
            //Sort high scores
            gameHighScores.sort(sortHighScores);
            
            //Display high score list
            if(gameHighScores.length > 0) {
                $("#game_high_scores ul").empty();
                
                for(let i=0; i<gameHighScores.length; i++) {
    		        $("#game_high_scores ul").append("<li>"+gameHighScores[i][0]+": "+gameHighScores[i][1]+"</li>");
    	        }
            }
        }
    }
}

//Sort high scores
function sortHighScores(a,b) {
    if(b[1] === a[1]) {
        return 0;
    } else {
        return (b[1] < a[1]) ? -1 : 1;
    }
}

//Boundry Class
class Boundry {
    constructor() {
        this.width = CanaryGame.GameCanvasWidth;
        this.height = CanaryGame.GameCanvasHeight;
        this.position = [0,0];
    
        CanaryGame.GameCanvasContext.beginPath();
        CanaryGame.GameCanvasContext.rect(0,0,this.width,this.height);
    }
}

//Ball Class
class Ball {
    constructor() {        
        this.initialize();
        
        this.updateCounter();
    }
    animate() {
        
        if(CanaryGame.GameOver) return;
        
        CanaryGame.GameCanvasContext.beginPath();
        
        CanaryGame.GameCanvasContext.arc(this.position[0], this.position[1], this.radius, 0, 2 * Math.PI);
        
        CanaryGame.GameCanvasContext.fillStyle = this.color;
        CanaryGame.GameCanvasContext.fill();
        
        //Check velocity for the ball
        if(this.position[0] - this.radius + this.velocity[0] < CanaryGame.GameBoundry.position[0] || this.position[0] + this.radius + this.velocity[0] > CanaryGame.GameBoundry.position[0] + CanaryGame.GameBoundry.width) {
            this.velocity[0] = -this.velocity[0];
        }
        
        if(this.position[1] + this.radius + this.velocity[1] > CanaryGame.GameBoundry.position[1] + CanaryGame.GameBoundry.height || this.position[1] - this.radius + this.velocity[1] < CanaryGame.GameBoundry.position[1]) {
            this.velocity[1] = -this.velocity[1];
        }
        
        this.position[0] += this.velocity[0];
        this.position[1] += this.velocity[1];
        
        //Check collision with the cursor
        let dist = Math.sqrt(Math.pow((this.position[0]-CanaryGame.GameCursor.position[0]),2) + Math.pow((this.position[1]-CanaryGame.GameCursor.position[1]),2));
        
        if(dist < 20) {
            gameOver(animation);
        }
    }
    initialize() {
        //Create a ball object
        this.color = this.randomColor();
        this.position = this.randomPosition();
        this.radius = this.randomRadius();
        this.velocity = this.randomVelocity();
        
        CanaryGame.GameCanvasContext.beginPath();
        
        CanaryGame.GameCanvasContext.arc(this.position[0], this.position[1], this.radius, 0, 2 * Math.PI);
        
        CanaryGame.GameCanvasContext.fillStyle = this.color;
        CanaryGame.GameCanvasContext.fill();
    }
    randomColor() {
        const colorArray = ["blue", "red", "yellow"];
        const randomNumber = Math.floor(Math.random() * 2);
        
        return colorArray[randomNumber];
    }
    randomPosition() {
        const xPosition = (Math.floor(Math.random() * CanaryGame.GameCanvasWidth))-5;
        const yPosition = (Math.floor(Math.random() * CanaryGame.GameCanvasHeight))-5;
        
        return [xPosition, yPosition]; 
    }
    randomRadius() {
        const size = (Math.floor(Math.random() * 5)+1)*5;
        
        return size;
    }
    randomVelocity() {
        const xVelocity = (Math.floor(Math.random() * 5)+1);
        const yVelocity = (Math.floor(Math.random() * 5)+1);
    
        return [xVelocity, yVelocity]; 
    }
    updateCounter() {
        $("#game_ball_counter span").text(Number($("#game_ball_counter span").text())+1);
    }
}

//Cursor Class
class Cursor {
    constructor() {        
        this.initialize();
    }
    animate() {
        //Move cursor as needed
        if(CanaryGame.GameOver) return;
        
        CanaryGame.GameCanvasContext.beginPath();
        
        CanaryGame.GameCanvasContext.moveTo(this.position[0]-20, this.position[1]-20);
        CanaryGame.GameCanvasContext.lineTo(this.position[0]+20, this.position[1]+20);
        CanaryGame.GameCanvasContext.stroke();
        
        CanaryGame.GameCanvasContext.moveTo(this.position[0]+20, this.position[1]-20);
        CanaryGame.GameCanvasContext.lineTo(this.position[0]-20, this.position[1]+20);
        CanaryGame.GameCanvasContext.stroke();
        
        //Set Position
        this.position = [this.position[0], this.position[1]];
    }
    initialize() {
        //Create a cursor object
        CanaryGame.GameCanvasContext.beginPath();
        
        CanaryGame.GameCanvasContext.moveTo(CanaryGame.GameBoundry.width/2, CanaryGame.GameBoundry.height/2);
        CanaryGame.GameCanvasContext.lineTo(CanaryGame.GameBoundry.width/2, CanaryGame.GameBoundry.height/2);
        CanaryGame.GameCanvasContext.stroke();
        
        CanaryGame.GameCanvasContext.moveTo(CanaryGame.GameBoundry.width/2, CanaryGame.GameBoundry.height/2);
        CanaryGame.GameCanvasContext.lineTo(CanaryGame.GameBoundry.width/2, CanaryGame.GameBoundry.height/2);
        CanaryGame.GameCanvasContext.stroke();
        
        //Set Position
        this.position = [CanaryGame.GameBoundry.width/2, CanaryGame.GameBoundry.height/2];
    }
}
